playstation

- 1.run Arjun at night to locate possible params; run over night
- 2.check nmap file for possible f5 big ip vuln
- 3.Read through kekka.js; looks to be some documentation

# GRAPHQL ENPOINTS
- GEThttps://web.np.playstation.com/api/graphql/v1/op?operationName=getPurchasedGameList&variables={"platform":["ps4","ps5"],"size":1000,"sortBy":"ACTIVE_DATE","sortDirection":"desc","subscriptionService":"PS_NOW"}&extensions={"persistedQuery":{"version":1,"sha256Hash":"00694ada3d374422aa34564e91a0589f23c5f52e0e9a703b19d065dedceb3496"}}
	URL ENCODED: https://web.np.playstation.com/api/graphql/v1/op?operationName=getPurchasedGameList&variables=%7B%22platform%22%3A%5B%22ps4%22%2C%22ps5%22%5D%2C%22size%22%3A1000%2C%22sortBy%22%3A%22ACTIVE_DATE%22%2C%22sortDirection%22%3A%22desc%22%2C%22subscriptionService%22%3A%22PS_NOW%22%7D&extensions=%7B%22persistedQuery%22%3A%7B%22version%22%3A1%2C%22sha256Hash%22%3A%2200694ada3d374422aa34564e91a0589f23c5f52e0e9a703b19d065dedceb3496%22%7D%7D

# the follow endpoint is pointing to a site that is parsing XML info; might be vulnerable to XXE
	-http://gs2-sec.ww.prod.dl.playstation.net/gs2-sec/appkgo/prod/CUSA17204_00/1/i_f5cb9329cace967eaefc3edbebc650783bb2c2eb79ad9ba04cc8902ea20e4875/i/pronunciation.xml

# http://tmdb.np.dl.playstation.net/ test
gives a 403 http://tmdb.np.dl.playstation.net/ run burp 403 bypass ext on it
gives a json api enpoint maybe http://tmdb.np.dl.playstation.net/tmdb2/CUSA00001_00_C32A667A087A40C2AD1889435907AA4B0D9DBA1F/CUSA00001_00.json

# read Ps4 updates 50 & 49 (latest) for possible attack surface

current google dork site:playstation.net inurl:dl

# Need to also test playstation remote! check https://remoteplay.dl.playstation.net/remoteplay/lang/en/index.html

https://auth.api.sonyentertainmentnetwork.com/login.jsp?request_locale=ja_JP ; 3 hidden fields
*go to a cma directory that returns a 403 and bypass it, bitch!

bruteforce https://www-stg.upro.playstation.net check also portal/FUZZ HERE as portal/c sets cookie
^ http://j.dl.playstation.net/
^ https://manuals.playstation.net/document/it/psp/2_80/menubar/tools.html
^ http://cma.dl.playstation.net/cma/lib/iepngfix.htc but run with a word list that has files!

custom 403 on https://apollo2.dl.playstation.net/mag
api endpoint https://remoteplay.dl.playstation.net/remoteplay/module/win/rp-version-win.json 

https://manuals.playstation.net/document/es/ps3/current/friends/addfriend.html?iframe=true&width=90%&height=90%
	*param of q=on[]= gives Invalid query was given ; possible sqli

# Possible path traversal
^ https://manuals.playstation.net/document/it/psp/2_80/menubar/tools.html

# looks like video files are fetched in multiple requests eg https://nsx.np.dl.playstation.net/nsx/material/4/4ce4028a2640576297f4ef19d6237cdf997cd4f2-1205200.mp4?_=1
	-above requests is completed after two request with these heards located in the first and second request respectavily
		1.bytes=13828096-
		2.bytes=10485760-
# endpoint accepts xml file check for xxe and look at wireshark data so you can get a example requests
http://static-resource.np.community.playstation.net/np/resource/title/NPWR00517_00/matching/NPWR00517_00-matching.xml

# information

surferbum618@gmail.com USER ID = 6380573630113222131 & Authorization: Bearer 3182a1a1-8bc9-4441-a6c8-51ceb7507537
