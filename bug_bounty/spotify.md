spotify

# misc info
asn 15169

# sitemaps
https://ads.spotify.com/en-US/sitemap.xml

# Possible IDOR
/*
endpoint: guc-spclient.spotify.com/remote-config-resolver/v2/configs/platforms/web/clients/open3/property-sets/cd2b35eea227afaf?installation-id=5f4542750c8288ffe806c46b38527556&fetch-type=delayed
response gives a group id of 329910 and 32841 in JSON object for kirra and surferbum users respectivly
*/

POST /rest/analytics/1.0/publish/bulk HTTP/1.1
Host: jira-aws.spotify.net
User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:86.0) Gecko/20100101 Firefox/86.0
Accept: application/json, text/javascript, */*; q=0.01
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Content-Type: application/json
X-Requested-With: XMLHttpRequest
Content-Length: 767
Origin: https://jira-aws.spotify.net
DNT: 1
Connection: close
Referer: https://jira-aws.spotify.net/secure/ConfigureReport!default.jspa
Cookie: AWSALB=N/vPl4vhZ8J4Xy85PmIgTVTaLKaZgnNGpGX+i3hC1W2UVSeNlLKnczWlU3aMC3bDWaxonCrIdA8fH3hLbj18zl3mM5SxuYJ2dzMzkkvMfKpJYhLqaZSr3z+vX/X9; AWSALBCORS=N/vPl4vhZ8J4Xy85PmIgTVTaLKaZgnNGpGX+i3hC1W2UVSeNlLKnczWlU3aMC3bDWaxonCrIdA8fH3hLbj18zl3mM5SxuYJ2dzMzkkvMfKpJYhLqaZSr3z+vX/X9; atlassian.xsrf.token=BYE9-LMXF-PDWU-Y3HB_07aa936489c41add8a1d18f87fca0dfd6eab6daf_lout; JSESSIONID=CD61876BF85768A28FFD7A01A497CB85

[{"name":"jira.navigation.header.dashboards.open","properties":{},"timeDelta":-3416},{"name":"browser.metrics.navigation","properties":{"apdex":"0.5","isInitial":"false","journeyId":"2cc9f658-cfe8-4acb-ab18-a548c13631ca","key":"jira.header.menu.dashboards","readyForUser":"574","threshold":"250","userAgent":"Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:86.0) Gecko/20100101 Firefox/86.0","applicationHash":"a6982cff65627fb3fa50669b736095827f0ea0a7","resourceTiming":"{\"☠\":[\"5,3,fp,s,i,i,i,i,i,i\"]}","userTimingRaw":"{\"marks\":{},\"measures\":{}}","experiments":"[]"},"timeDelta":-2835},{"name":"jira.navigation.header.dashboards.close","properties":{},"timeDelta":-1238},{"name":"jira.navigation.header.dashboards.item.click","properties":{},"timeDelta":-1201}]

# user I’d cookie
kirradawg:
Cookie: sp_t=cb2a89cec9239ceeb1bd6713cbcd4400;
OptanonConsent=isIABGlobal=false&datestamp=Thu+Feb+18+2021+23%3A51%3A11+GMT-0600+(Central+Standard+Time)&version=6.13.0&hosts=&consentId=ce4f7e43-752a-4c34-986b-c9b4d0b18e7d&interactionCount=1&landingPath=NotLandingPage&groups=t00%3A0%2Cs00%3A1%2Cf00%3A1%2Cm00%3A1%2Ci00%3A1&AwaitingReconsent=false;
_pin_unauth=dWlkPU9ESmpNRFk0TVRNdE1UbGxOeTAwWkRJMExXSmhaREV0T0dSak5EZ3lNV1ExWTJKbA;
_ga=GA1.2.1285157531.1613690265; _gid=GA1.2.1524484081.1613690265;
_fbp=fb.1.1613690264874.1185535636;
_scid=bc79e8ca-be92-4b12-a48c-cd0917ba12df; _sctr=1|1613628000000; sp_m=us;
sp_landing=https%3A%2F%2Fsupport.spotify.com%2Fus%2Fusing_spotify%2Fsearch_play%2Fenable-the-spotify-web-player%2F%3Futm_source%3Dopen%26utm_campaign%3Ddrm_error%26utm_medium%3Dweb;
sp_landingref=https%3A%2F%2Fopen.spotify.com%2F;
sp_last_utm=%7B%22utm_campaign%22%3A%22drm_error%22%2C%22utm_medium%22%3A%22web%22%2C%22utm_source%22%3A%22open%22%7D;
_rdt_uuid=1613691158520.080f5abf-9fb6-463d-a6df-27fc8311daeb; _hjTLDTest=1;
_hjid=5d531d0f-6371-44e4-a9a5-695fbe9bdd62; _gat_UA-5784146-31=1; _gat=1;
sp_dc=AQAmmj5T6YYf9TR5rXTY63uRxwJ1nPTmEcyPLdQrHuQT4wUKkJYyy5vwOuzYf12n39iBxrkvfCAx_x84yXIKm39UQBSrZ-RdZD2E6rVSww;
sp_key=448d0d72-ce09-409e-abe2-43e33ddee9ee

surferbum618
Cookie: sp_t=5f4542750c8288ffe806c46b38527556;
OptanonConsent=isIABGlobal=false&datestamp=Thu+Feb+18+2021+23%3A51%3A16+GMT-0600+(Central+Standard+Time)&version=6.13.0&hosts=&consentId=6b1fc322-5ef8-4441-a139-8bcda1027872&interactionCount=1&landingPath=https%3A%2F%2Fopen.spotify.com%2F&groups=t00%3A0%2Cs00%3A1%2Cf00%3A1%2Cm00%3A1%2Ci00%3A1;
_ga=GA1.2.497092844.1613690278; _gid=GA1.2.1613768490.1613690278;
_pin_unauth=dWlkPU1tTTBaVGMwTldVdE9HSmtZeTAwTnpFekxXRmhNMlF0TldGaFpUTXpNVFU0TkRjeQ;
_scid=a9d95626-c397-4c36-9e9a-96c2cf30a722;
_fbp=fb.1.1613690278559.1518717260; _sctr=1|1613628000000; _gat=1;
sp_dc=AQD8Qeh1s565BITFaCc8VRKuijIsIGi7k3zNATMLDI4HgObC4zfsfyEMBn6M1LA10U0KdxwJHucnkF6skxQZLWB2OqhE6DILjgZHgJmhAQ;
sp_key=ddfdfb2e-3b1b-417f-b7b6-3b63dd504592

# JWT Token
kirraDAWG (privlidged user)
authorization: Bearer BQDpoiD9JZzzDkOFHs9V3wYnZdbUQjVtw5rUrVh9cFdweGcWLvPKlds3irU5mRXy3SpvPFa6PgQ82qnoQQf8qYDcQepte8d8Lg71_GS0bJt7EVtNdBVlxcrLT-hgmKUuOKIgXGamCaiI_TU9K1d-vf4gSrCWa7syIRYcDHHxJ3NHmt_hOuEqztG2jDjPfaWIW-KypFhzGOl-L0bO9koLBSXOAkclLkEj9traLLI6t2m61ufTiU-Z6SGddWxgOm95sTLPFHXEc_r8NKZ5uYJaV968GdmTYlcwCw1N7f7FXt3w8wK-k2uyKGMsp-IZ

surferbum618 (low level user)
authorization: Bearer BQDxwkiXimfxqSeRtRnkG_Yk4LhrbYT3v210MTyL1SUxEZz1vLXxl814vNGVcOhv-qjpe4jKgYr-BOPIUoL58FAIY7wYGBL2t66lJz0ZrfgrKvZwHn2deQ62YSrdTAPufw7VOV7XXQP4_WZZkggNGUtn4E_PifC6KZRWl3RFlrsA-UWX-k3gHuTrnh5w-14Ld92pHoJ0uXvmdk-tjRl5KpeSnGqhVXD_afSao_NrPRfm_3AHZCdMqlpMHwA-hBTXSAnvYSnzjuOr1ATsxHzfrErZaU_ARlamDhoVRrM


# htaccess rules
 BEGIN WordPress # The directives (lines) between "BEGIN WordPress" and "END WordPress" are # dynamically generated, and should only be modified via WordPress filters. # Any changes to the directives between these markers will be overwritten. RewriteEngine On RewriteRule .* - [E=HTTP_AUTHORIZATION:%{HTTP:Authorization}] RewriteBase / RewriteRule ^index\.php$ - [L] RewriteCond %{REQUEST_FILENAME} !-f RewriteCond %{REQUEST_FILENAME} !-d RewriteRule . /index.php [L] # END WordPress

# possible XXE
## 403 but is of content-type xml
https://35.227.201.204/

# api.spotify.com
- 429 status code means too many requests
