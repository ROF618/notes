greenhouse

# https://go.greenhouse.io/uncubed? 
	-looks like an api endpoint; check for graphql endpoint

# check params for https://www.greenhouse.io/integrations/yext?partnername=&partneremail=

# possible graphql endpoint at events.greenhouse.io; need to check


#private.api.greenhouse.io uses prometheus monitoring system
	-end point is found at https://private.api.greenhouse.io/metrics ; need to go through burp proxy as no ssl cert is provided for that directory
	-try /metrics on other directories
	-youtube video hinted that you might need certain parameters to be able to scrape data from http endpoint
