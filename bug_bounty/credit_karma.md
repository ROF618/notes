credit_karma

<u>Possible API enpoint</u>
https://ivr.test.creditkarma.com
	- returning 404 json response

*<u>Check for 403 Bypasses</u>
https://grafana-prod-ck-bulk-usc1.paas.creditkarma.com/
https://alerts-prod-ck-bulk-usc1.paas.creditkarma.com/

*<u>Need to Bruteforce</u>
https://api-fg-origin.us-central1.creditkarma.com/ 
	- returns a 403

<u>Dev Comments</u>
// TODO: https://dev-segments.static.corp.creditkarma.com/services/config/
                // may be a good replacement for env detection vs. passing in through e.data.IS_PRODUCTION
		
<u>Session Management</u>
When registering a new user, said user is prompted with a series of questions to verify identity. The following cookie (CKSESSID=0eaa38mo40abadmshkilc37php;) is the only required cookie to be sent to the following endpoint to get the registration form for that particular user https://www.creditkarma.com/registration/creditsignup/

<u>IDOR</u>
https://creditkarmacdn-a.akamaihd.net/ckfiles.com/assets/950686430647/res/out/myprofile.js

- 1.run amass with shodan key
- 2.create wordlist from tomnomnom's video
- 3.look at possible_robots.txt file
- 4.go through ww1.creditkarama.com with burp suite
- 5.check credit karma on crunchbase to see past acquisitions
- 6.check intersting_doms.txt for new places to check out
- 7.pull key words from "out" directory to add to personal word list
- 8.go through probed list/screenshots and make sure you get all relevant information
- 9.fingerprint web server and start to identify tech used on apps
- 10.add all thre months
- 1.attempt to exploit dom xss on the function found in the xss strike files
- 2.use authorize burp ext to search for idors; walk site with authorize on; see stoks video help needed
- 3.set up credit karma creds on hacker1; if it doesnt work try bug crowd

