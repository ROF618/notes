- Once you've logged in, search your password in Burp history. Make sure your plaintext password isn't being sent anywhere it shouldn't (like a logging service).
- Perform a forgot password request. Just before you reset it, search Burp history for the token/secret that it sent you. If it's there, you've got an account takeover.
- When you get to a MFA request, check if you can bypass it by simply browsing directly to the dashboard without entering a code.
- See if you can generate multiple valid OTPs. This might allow you to feasibly brute-force MFA, even if there are sensible rate-limits/lockouts applied.

reference: https://x.com/hakluke/status/1851471765790433323
