sanebox

#mail.sanebox.com GET
-Cookie: CookieControl={"<b>necessaryCookies</b>":["language","amazon-pay-connectedAuth","amazon_Login_state_cache","amazon_Login_accessToken","slides_completed","video_finished","sb-digest-schedule","timezone_ignored"],"optionalCookies":{"gdpr-ga":"accepted","gdpr-yt":"accepted","gdpr-intercom":"accepted","gdpr-ant":"accepted"},"statement":{},"consentDate":1603658844493,"consentExpiry":90,"interactedWith":true,"user":"93C5A7A8-6F21-4CC6-B2FE-C8208EBB8AC0"}; _ga=GA1.2.1639873621.1603658818; intercom-id-gtnuihbd=0141e4e9-215e-4fb5-adf8-1005ec499175

#possible idor

##https://www.sanebox.com/api/v3/mailboxes/790881/9 GET
- _sbs_sane=85d05232dc13e96bcfc76b8d70c0be88; this is the auth token
- anti csrf token is not needed for this API call
* need to create another user and then compare number generated for the get requests

#site map
-this site map has timestamps https://blog.sanebox.com/sitemap-1.xml but looks to be strictly for blog subdomain

#logs & errors
-http://daveshort.org/sanebox/sanebox/changelog.txt seems to be a change log of updated versions of sanebox

#Misc info
sanebox.com/analytics/page_views uses post request to send up user agent info in json format cookie and body of request found below:
Cookie: state=IntcInN0YXRlXCI6XCJyZXR1cm5pbmdfdmlzaXRvclwiLFwidmlzaXRzXCI6MixcImlkZW50aXR5XCI6XCI4Mjg3YzdlNmNlNGIyOWM5YTYxZGM0YzYzNDZmNzRjN1wiLFwibGFuZGluZ1wiOlwiaG9tZXBhZ2VfdjgyL3dlbGNvbWVcIn0i--5a2bf26dd4f423a5ebe1ed3c1905fae2122ba65c; source=hackerone.com; CookieControl={"necessaryCookies":["language","amazon-pay-connectedAuth","amazon_Login_state_cache","amazon_Login_accessToken","slides_completed","video_finished","sb-digest-schedule","timezone_ignored"],"optionalCookies":{"gdpr-ga":"accepted","gdpr-yt":"accepted","gdpr-intercom":"accepted","gdpr-ant":"accepted"},"statement":{},"consentDate":1603732589970,"consentExpiry":90,"interactedWith":true,"user":"9E2663FA-EEED-403D-B403-D68BBB4C9037"}; intercom-id-gtnuihbd=9be08a99-61a5-4fd3-8f5b-fa979b372734; intercom-session-gtnuihbd=; _ga=GA1.2.402923141.1603732524; _sbs_sane=85d05232dc13e96bcfc76b8d70c0be88



{"href":"https://www.sanebox.com/","browser":"Firefox","browser_version":"82.0","operating_system":"Linux","operating_system_version":"x86_64","screen_height":768,"screen_width":1366}
