#!/usr/bin/env bash

fecha=`date +%F_%H`

cd ~/notes

pid=$!

git pull

wait $pid

git add .
wait $pid

git commit -m "auto push $fecha"
wait $pid

if [ -n "$(git status - procelain)" ];
then
    git push -u origin master
else
    echo "No changes to push"
fi
