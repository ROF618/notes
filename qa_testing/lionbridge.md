lionbridge

# rating methodology
1. understand the query e.g. decide whether it is a know, know simple, visit in person, do
2. evaluate the result blocks
3. choose a result set, assign a rating
4. explain your rating with a comment

# make a web scrapper app that gets how long task should take and then gives you a coutdown until that time is up

SERP: Search Engine Results page
EAT: Expertise Authoritativeness and Trustworthiness
YMYL: Your Money, Your Life
SCRB: Special Content Result Block
WSRB: Web Search Result Block
DARB: Device Action Result Block


# Look for first:

1. determine if the page is a YMYL 
	a. news and current events
	b. civics, government and law
	c. finance
	d. shopping
	e. health and saftey
	f. groups of people e.g. race
	g. other: fitness and nutrition, housing information, choosing a college, finding a job, etc.
Please use your judgment
2. Find the homepage (page 12 of guidelines)
3. determine the following:
	a. purpose of the page
	b. expertise, authoritativness, trustworthiness (EAT)
	c. main content's quality and amount of content
	d. who is responsible for producing this information
	e. website reputation


# Finding Reputation
1. identify homepage/identify creator of the site
2. google dork for site -[main site actually testing]
3. look for articles/reviews written about the site
4. look for wikipedia articles on the site/content creator


# Page content rating 

## STEPS WHEN ACCESSING PQ
	1. understand true purpose of the site
	2. assess if the page has benefit for the user
	3. research EAT
	4. check for MC copyright infringements
	5. for shopping websites do special checks; look for contact info including the store's policies on payment, exchanges, and returns (sometimes listed under "customer service")

## High
	* satisfying amount of High quality MC
	* clear information about who created the site
	* reputation (page can be rated high without reputation but never high with a negative reputation)

## Highest
	* difference between highest and high is based on the quality of MC and the reputation
	* needs a very high level of EAT
	* very satisfying amount of high MC
	* very positive reputation

## Low
	* intended to serve a beneficial purpose but didn't achieve it
	* inadequate level of EAT
	* low quality MC/not enough MC
	* title is clickbait
	* distracting ads e.g. sexually suggestive or disturbing ads

## Lowest
	* EAT is of the lowest quality
	* little to no MC
	* inaccurate information
	* broken functionality 
	* MC is copied from an outside source
	* auto generated content from RSS feeds or api data
	* obstructed or inaccessible MC
	* unmaintained or defaced websites
	* is harmful or promotes harm 

# Understanding Mobile Users



# Needs Met

## fully met
	* query must be specific and the user needs are full met
	* can be used when a user is looking for a specific website and the result block gives them that website
	* can be used when a user is trying to accomplish an action on their mobile and the block executes what they wanted
	* make sure to check that the query is answered by reputable source before assigning this to query
	* the user misspelling the query does not disqualify the "fully met" rating if the intent is obvious

## highly meets
	* this rating is assigned to a query that is a bit broad but the results accomplish what most people are looking for

## moderately meets 109
	* assigned to a query that is somewhat satisfying to most users or very satisfying to a few users

## slightly meets
	* use this for out of date information or exaggerated or misleading titles

## fails to meet
	* unrelated to the query or highly inaccurate
	* use for results that harm or intended to harm users
	* impersonating another website
	* factually inacurrate

## rating porn queries
	* check page 129
	* assign the porn flag to all queries that contain porn regardless of intent
	* for possible porn intent queries treat as if they did not mean to find porn
	* all rating criteria still applies when rating a porn query; look at intent

## rating foreign languages
	* assign the foreign language if most users wouldn't understand the language but you do
	* if the query is asking for a video or a song in a foreign language assume that the results should be in that language

## did not load flag
	* assign if the LANDING page did not load, not the result block
	* do not use for the following: malware page, pages that have removed or expired MC, in accessible because you need a subscription
	* reserve this flag only for when a page does not load or times out

## upsetting content flag
	* promotes harm towards a group of people
	* presents graphic violence with context or a beneficial purpose
	* explicit how-to for things like human trafficking 
	* use this flag for all upsetting content regardless of the users intent
	* still rate the needs met based on perceived intent

## name queries
	* do not assume the query was misspelled

# FACT CHECKING SITES
snopes.com

# https://geoworkerz.myabsorb.com/#/resetpassword?link=aHR0cHM6Ly9nZW93b3JrZXJ6Lm15YWJzb3JiLmNvbS9hcGkvcmVzdC92Mi9wYXNzd29yZC1yZXNldHMvWmpTc0VjZnBBZUVKcDFFdXNqOW1zTExSY19UckhDTnhLOHdueTdZN1FpZWpBam1iQ3RLZU5Db002emVIdW5HRHZJamg3VVREMXJ2dG1XM2VyYmw2X1Ey password

Ulti
username: bravoc1
pw:PuenteLeon2021!


# Side by Side learn/SXS training
## comment given when rating side by side
- first describe user intent
- now describe why you prefer one over the other
- describe any particular result that made you choose one over the other
- a side with helpful SCRB's will generally be better than a side with out them
- position of results (like in other rating aspects) highly effect whether one side is better than the other
- usually when each side has an SCRB but one SCRB has a list and the other side has a single entity then the list side generally should be rated as "Much Better"
    * when the query is list seeking then the results that give the users the most number of accurate choices should be rated highest
- when the query has multiple user intent remember that the side that addresses the most of those user intents should be rated higher on the SxS rating scale
- make sure to take into consideration the diversity of sources when rating
- results should appear in order of helpfulness
