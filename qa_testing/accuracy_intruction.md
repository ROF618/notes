Severity Factors

“Severity” is subjective, but the following non-exhaustive list of diagnostic questions may help you think about the degrees and types of issues related to accuracy that you may find when reviewing the Target Sentence:

Is the issue directly related to the primary intent of the User Query? That is, does the Target Sentence give an inaccurate or unverifiable answer to a user’s explicit question? Inaccuracies directly impacting the user's question are typically severe.

Would the inaccurate or unverifiable claim influence the user’s decision and/or potentially lead to negative consequences (e.g., harm, societal conflict, wasted time or money, or even annoyance)? Such claims would be generally be considered high severity.

Is there a clear-cut (obvious or blatant) issue with the inaccurate or unverifiable claim (e.g., an incorrect date for a recent event)? Would it be embarrassing or outlandish to state such a thing? A clear-cut inaccurate claim directly addressing the user's intent is a more severe error than a clearly inaccurate claim that is less relevant. Embarrassing or outlandish claims that undermine the credibility of the response are generally problematic.

Is there hedging language (e.g., “seems”, “may/can”, “usually/typically”) or use of ranges (e.g., “the length is between 10 to 15 feet”) that softens the definitiveness of the claim? A claim with reasonable hedging language might make a claim meaningfully less problematic. 

Is there approximation or rounding (e.g., “circular” instead of “elliptical”; or “2 grams” instead of “2.3 grams”)? Note that rounding can be hedged by words such as “about”; this might make the claim more acceptable in some but not all cases. For example, in the case of dosage for a medication, there may be a higher expectation of precision.

What is the relative scale of the issue? For example, being off by one meter could be worse when talking about the height of a statue as compared to when talking about the height of a building.

Is there any moderating context or nuance? For example, a claim that “horchata originated in Spain” is technically inaccurate (the drink has earlier roots in Africa), but if the focus of the Response is the introduction of the drink to Latin America, then the claim may be more acceptable in context.

Does the inaccurate or unverifiable claim involve a sensitive or controversial topic (e.g., religion, politics, fringe theories) or involve someone’s personal information (e.g., family status, legal history)? Inaccurate claims of this nature are typically more serious.

What is the expectation of freshness? For example, a claim that “the Patriots have won five Super Bowls, with their last win in 2017” appears to be based on excessively stale information, given that there is only one Super Bowl per year and the sixth and last victory by the Patriots was in 2019.

In some cases, you will be asked to indicate which of the above factors influenced your rating.
Flags
Please use the flags described below to indicate rare cases that prevent you from assigning a rating: 

Every claim in the Target Sentence is actually accurate.
Use this flag when you are convinced that the Target Sentence should not have been sent to this task because every claim is actually accurate.

The Target Sentence and/or User Query is impossible to understand.

There is a formatting or data preparation issue (e.g., truncated text or a table or mathematical expression that does not render properly) that makes it impossible to complete the task.

Suggested Procedure

Read the Target Sentence carefully, using the User Query and full Response as context to aid your interpretation. Identify the claim(s) being made by the Target Sentence, and try to identify the location(s) and nature(s) of any inaccurate claim(s).

If it is difficult to find any inaccuracy, you may use a search engine to research the claim(s) being made by the Target Sentence. Always try to rely on reputable information from trustworthy websites or creators with a high degree of expertise, experience, and/or authority for their content. When researching claims that have the potential to be harmful if inaccurate, you should have an even higher standard for considering a source to be reputable.

In some cases, you will be shown comments from raters who had flagged the statement for suspected inaccuracies in a different task. The comments may help you pinpoint inaccuracies that are not obvious. Note, do not assume the comments are true as it is possible that they contain mistakes.
If any exceptional case is applicable, please check the appropriate Flag(s) and skip to the Explanation. Otherwise, proceed with your Severity rating and, if prompted to do so, indicate the factor(s) that influenced your rating.

Write an explanation for your rating.


