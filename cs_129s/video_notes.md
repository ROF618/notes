video_notes

# Video 1

-Netflix uses a service called chaos monkey that purposely breaks things in order to see where potential vulns are

# Video 2
- REST API uses directories as params for get request so instead of seeing ...com/auth/users?id=1234 the RESTful aproach uses ...com/auth/users/1234

# Video 3
-java servlet is a java app that recieves http requests from clients and sends out the appropriate http response
-java web container is a platform or an engine that provides a java run time enviornment for a web app eg apache tomcat
-pinned ssl cert sites make sure the cert is the correct one regardless where the source of the cert is issued eg even if you had a root cert from one of the trusted cert issuers you could not MIM and decrypt https traffic as you dont have the exact cert its looking for

# Video 4
-change user agent to google bot UA and you'll see what google sees. Usually has a lot more stuff than the normal site
-if you see x-forwarded-header that usually means that the requests is going through a load balancer

# Video 5
-you can disable all js running client side with burp and burp will force the client to show all hidden fields as well (see methodology for more info)
-if you get a response that looks to be encoded try elimenating the accept encoding header
-you can remove all cookies in burp under proxy>options>match and replace
-how do you check to see if a seralized value has been signed?
-if your're getting a java searlization response use DS'er to deseralize the data

# Video 7
-if a cookie is set for a top lvl domain like blog.com well then stealing cookies would allow you to login to any of the subdomains e.g. sam.blog.com
-consider reporting if sensitive information is found in a user token
-if you find a token has a detecable structer then it could be vulnerable to attack like guessing or predicting future tokens
-hidden fields are used to verify that the user is comming from a specific url or page

# project w201
	-sudo rm /var/www/html/index.html

# project 9
	-html code: 6LdcUOgZAAAAADVDau4kL6PWDU0if1rb_LsPMagP
	-secret key for com: 6LdcUOgZAAAAADEBgzIlLPIoL0TzYeeeGACw1-ep
	
# Video 10
-be on the look out for anything that looks to be calling a system command when looking at pages that take user input modify that page 
	* try to add the following to user input
		1.`
		2.;
		3.|
		4.&
	* also path traversal vuln should be checked
-when uploading images check to see if you can upload files that are not images
	* if .jpg or .png are only allowed try inserting a null byte before .ext e.g. mal.file%00.jpg
