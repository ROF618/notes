bbh_z


#FFv2 challenge
* https://bugbountytraining.com/assets/js/custom-script.js
	-this is where the logic for the bug is found 1:16 pm

* looks like https://www.bugbountytraining.com/FFHv2/api/loader.php?f=/invites.php is white listed; not 100% sure though
	-above url only provides a 0 but it injects the zero via innerHTML prop possible XSS there

* index.php?act=login POST req verifies creds and calls GET method on index.php?act=login when incorrect creds are provided

* https://www.bugbountytraining.com/FFHv2/index.php
	-when you hit the request credentials it allows you to load another request from the same domain; think about how this can be exploited
* when the request invite function makes the call to the /api/invite.php endpoint; use intercept to hit /testing.php or admin.php to see if there is any difference when hit
