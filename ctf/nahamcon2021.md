nahamcon2021

#tweet flag
flag{e36bc5a67dd2fe5f33b62123f78fbcef}


#echo challeng
13/3/2021 4:15 am ` ls ../ ` gives the flag.txt file but you cant cat the file

Working payload `< ../flag.txt` #it gets around the character limit

cat ../flag.txt      < ../flag.txt

#imposter
username - admin 
email - admin@congon4tor.me


#banking on it
chris card id - 72620266

clerk = cl3rk new card id is 13061382


-rw-r--r-- 1 root root 2319 Feb 25  2020 /etc/bash.bashrc
-rw-r--r-- 1 root root 3771 Feb 25  2020 /etc/skel/.bashrc
-rw-r--r-- 1 root root 807 Feb 25  2020 /etc/skel/.profile
-rw-r--r-- 1 root root 3106 Aug 14  2019 /usr/share/base-files/dot.bashrc

====================================( Interesting Files )=====================================
[+] SUID - Check easy privesc, exploits and write perms
[i] https://book.hacktricks.xyz/linux-unix/privilege-escalation#sudo-and-suid
strace Not Found
-rwsr-xr-x 1 root root        67K May 28  2020 /usr/bin/passwd  --->  Apple_Mac_OSX(03-2006)/Solaris_8/9(12-2004)/SPARC_8/9/Sun_Solaris_
2.3_to_2.5.1(02-1997)
-rwsr-xr-x 1 root root        44K May 28  2020 /usr/bin/newgrp  --->  HP-UX_10.20
-rwsr-xr-x 1 root root        87K May 28  2020 /usr/bin/gpasswd
-rwsr-xr-x 1 root root        52K May 28  2020 /usr/bin/chsh
-rwsr-xr-x 1 root root        84K May 28  2020 /usr/bin/chfn  --->  SuSE_9.3/10
-rwsr-xr-- 1 root messagebus  51K Jun 11  2020 /usr/lib/dbus-1.0/dbus-daemon-launch-helper
-rwsr-xr-x 1 root root        39K Jul 21  2020 /usr/bin/umount  --->  BSD/Linux(08-1996)
-rwsr-xr-x 1 root root        67K Jul 21  2020 /usr/bin/su
-rwsr-xr-x 1 root root        55K Jul 21  2020 /usr/bin/mount  --->  Apple_Mac_OSX(Lion)_Kernel_xnu-1699.32.7_except_xnu-1699.24.8
-rwsr-xr-x 1 root root       163K Jan 19 14:21 /usr/bin/sudo  --->  /sudo$
-rwsr-xr-x 1 root root       463K Mar  9 14:17 /usr/lib/openssh/ssh-keysign


[+] Searching uncommon passwd files (splunk)
passwd file: /etc/pam.d/passwd
passwd file: /usr/bin/passwd
passwd file: /usr/share/lintian/overrides/passwd
