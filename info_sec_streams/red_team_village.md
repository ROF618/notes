red_team_village

#Harsh Bothra Attacking Jira instances

- jira.*target*.com is a valid jira instance where *subdom*.atlassian.net is the company site and it does not fall into BB

## steps when attacking
1. identify custom jira implementation
2. check for the jira version
3. search for jira cve using mitre/open search
