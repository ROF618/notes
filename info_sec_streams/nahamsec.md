nahamsec

# naffy stream

nmap -T 4 -iL hosts -Pn --script=http-title -p80,4443,4080,443 --open

- first look for a deviation from the normal requests and then ask yourself
  why is it doing that

# HADDIX's STREAM

## 12.12.2019

* always check for a robots.txt file if you have access to web server
	- This file tells webcrawler like google and yahoo to not index these pages/file 
	- This is important because this is usally a list of files that devs do not want you to go to

## What can Burp help me with?


## Target
	* Focus on specific sites by clicking on specific tree's or domains and see all the request you've sent
	* Focus on specific functions
	* Visualize attack surface a.k.a. make a mental note of all possible areas where burp has show possible vunerabilities
	* Set "scope" to filter all other tools and OOS items

## Proxy
	* Trap/modify live traffic
	* View all traffic
	* Set wide scale configuration for the traffic flowing through Burp

## Repeater
	* Request things quickly and then tamper with the request and test 
	* Performs manual test

## Intruder
	* Check SecList git hub to throw into intruder 
		-Good to use in Burp
	* The double S found in the position tab lets you know where the Intruder tool will inject the payloads
	* The payload can be switched in the drop down menu EG you can make Intruder iterate through numbers 

## Sequencer
	* Tool that allows you to request cookies from an app and the analyze said cookies

## When to Fuzz?
	* When you have elicited an error or you see reflection in an app EG when you give input and you see that input in another section of the app
	* Parameter that you think deal with a dtabase query & you have *hunch* are vulnerable
	* When you the source
	* When you are regression testing


# 22.01.2020

## first steps in recon
	* find ASYN number
		-Yassine Aboukir ASYN lookup tool is a good tool

	* Content Discovery
	As a newbie, what convention/workshop would you recommend to attend when you're trying to get better as a bug bounty hunter
	* check out thes tools
		-github-dorks: repo has why the tools used in recon search for what they search for
		-trufflehog
		-google/wikipedia is good to way to find information about associated companies
		-amass
		-subfinder
		-massdns
		-ffuf
		-turbo intruder(burp extension)
	* Access control issues are something you need to learn and learn well
		





# NAHAMSEC STREAM


## 12.12.2019

* always check for a robots.txt file if you have access to web server
	- This file tells webcrawler like google and yahoo to not index these pages/file 
	- This is important because this is usally a list of files that devs do not want you to go to

## What can Burp help me with?


## Target
	* Focus on specific sites by clicking on specific tree's or domains and see all the request you've sent
	* Focus on specific functions
	* Visualize attack surface a.k.a. make a mental note of all possible areas where burp has show possible vunerabilities
	* Set "scope" to filter all other tools and OOS items

## Proxy
	* Trap/modify live traffic
	* View all traffic
	* Set wide scale configuration for the traffic flowing through Burp

## Repeater
	* Request things quickly and then tamper with the request and test 
	* Performs manual test

## Intruder
	* Check SecList git hub to throw into intruder 
		-Good to use in Burp
	* The double S found in the position tab lets you know where the Intruder tool will inject the payloads
	* The payload can be switched in the drop down menu EG you can make Intruder iterate through numbers 

## Sequencer
	* Tool that allows you to request cookies from an app and the analyze said cookies

## When to Fuzz?
	* When you have elicited an error or you see reflection in an app EG when you give input and you see that input in another section of the app
	* Parameter that you think deal with a dtabase query & you have *hunch* are vulnerable
	* When you the source
	* When you are regression testing


22.01.2020 Nahamsec STREAM

## first steps in recon
	* find ASYN number
		-Yassine Aboukir ASYN lookup tool is a good tool

	* Content Discovery
	As a newbie, what convention/workshop would you recommend to attend when you're trying to get better as a bug bounty hunter
	* check out thes tools
		-github-dorks: repo has why the tools used in recon search for what they search for
		-trufflehog
		-google/wikipedia is good to way to find information about associated companies
		-amass
		-subfinder
		-massdns
		-ffuf
	* Access control issues are something you need to learn and learn well
	

## 29.1.2020 HADDIX STREAM

### blind xss
 * You can use local or server side tools that will alert you if the blind xss was executed followed by some useful information
 * Most CMS like wordpress and Drupal have protections against XSS attacks


 * When looking to fill in credit card info look to use cc generator apps online
 * Possible VPN IPmanage
 * Why not pipe the burp traffic through a ssh socks proxy connection to a droplet as an alternative to the vpn?
 	* another way around getting banned is to go to the origin or to specify an upstream proxy server in burp

## Analyze target in Burp Suite
	*thats where you can check where the endpoints are for request sent while walking the site

## BURP SCANNER
	* dont check anthing thats client side
	* input returned could help you find endpoints to use for xss
	* File upload needs to be checked
		* PHP has a problems with command injection; make sure to always check when PHP is concerene

!USE ZAPP its an open source BWAPP

* JSPARSER and Link finder tools for alternatives to BURPS spider	


## 7.3.2020

Haddix Stream

### Assets

	-Look up recently bought companys and look for bugs on those newly aquired assets of the current program your working on

### General Info
	-Make templates for all report Vulns e.g. XSS, CSRF
	-Content Discovery use ffuf aka directory brute forcer



# NAHAMSEC STREAM

## 15.3.2019 

### RECON
	1. Walk the site first and understand how the site works
	2. Then you can run recon tools

## 3.5.2020 NAHAMSEC: NAFFY

### RECON
	1. get domains a subdomains
	2. run nmap to see what's open
		Naffy's nmap: nmap -T4 -iL hosts -Pn --script=http-title- p80,4443,4080,443 --open


#Def Con 2020

## Haddix presentation BHMv4 RECON

-look up subdomainizer by neeraj edwards for parsing javascipt files
-look up gwendal le coguic github tool for finding stuff on github
- shosubgo uses shodan api key for subdomain enumeration and gives different results than amass * make a recon script that chains amass and other tools
- for akamai pages try origin-sub.domain.com or origin.sub.domain.com to bypass the filtering by going to the source also try ww2.$target.com
- nuclei for subdomain takeover checks

### Haddix BHM Workshop

1. Define and at a high level define scope
2. Run amass intel on asn if available
3. Reverse whois lookup on domian or company name
	- When checking URL's even if page is blank but favicon is shown, that means that website is resolving
4. Google foo/dorks time
5. Start the the sub-domain enum!
6. Probe list of domains and subdomains
7. Manually check the probed list
8. Port scan raw sub and top level domains for existing services
9. Start scan
10. BURP BOUNTY CREATE NEW RULE CALLED BLIND XSS TO APPEND TO EVERY REQUEST your xss hunter payload
xiddahjToday at 4:57 PM
https://gist.github.com/jhaddix/77253cea49bf4bd4bfd5d384a37ce7a4


# PARANOIDS STREAM
Can you speak about collaboration? Like whats the best way to find a mutually beneficial collaboration? What should you communicate when looking for someone to collaborate with?


# RECONLESS RON'S 1PASSWORD
1. use app as intended and plan out workflow for when the app works corectly
2. identify how we can break out of that workflow to compromise the security of users or the app itself


# Nahamsec stream @ Mason Hicks
-use auto repeater for replacing parameters with input like single quote

# 1_11_2020 bit_k
-make sure you focus on interaction
-keep track of small bugs, like open redirects
-check out pwnfox

#27_11_2020 Apple crew
-hackvertor is a burp ext that ZIOT uses to decode or incode user text

# zseano 7.3.2021
- CHECK FOR PROGRAMS THAT HAVE MULTIPLE WRITE UPS ON THEM


# shubs 18.4.2021 currently @ 1:50 on video, pick up notes from there

- Created kr because Content Discovery hasn't addressed the problem of how APIs and their frameworks have been changing over the years
- KR has two modes:
    1. scan mode
    2. brute mode
        * brute mode is more of a traditional take on content discovery
- Assetnote team has put together a list of signatures/fingerprints for most APIs & their frameworks. These signatures are located in the signatures direcory of the KR repository
- use the brute mode in KR when you have a large number of host and you want to through a generic word list at it
- Assetnote team combed through various api frameworks in order determine how to get certain endpoints
- routes-small & routes-large are compiled from all the swagger and open api specifications gathered during Assetnote's research
    * these files tell KR what type of requests it should send e.g. POST, GET, DELETE
- if KR is run without the full scan flag, KR will pick a random route in an attempt to cut down on the time it takes to run KR
    * if KR finds an API route that was succesfull then it will try all of the other routes
- KR is also able to replay requests through Burp Suite
- python2 censys_io.py --api_id "$CENSYS_API_KEY" --api_secret "$CENSYS_API_SECRET" '(80.http.get.headers.x_powered_by:Express OR 443.https.get.headers.x_powered_by:Express) AND 443.https.tls.certificate.parsed.subject.organization.raw: "Google LLC"' -f ip | sed -e 's/u//g' | sed -e "s/'/\"/g"| jq '.ip' -r | pbcopy
- -A is where you define your wordlist
- KR by default tries a random route and if something hits then it tries every route in the word list
- You can copy requests found in KR results and feed it back into KR  command and then get further data on that specific requests
- KR was designed to take big scope lists and discover content at scale
- benefit of KR is that it goes the extra distance when doing content discover e.g. when you hit an api enpoint that returns a 301 instead of 302 it knows to keep diging there.
- KR also takes the extra effort of having to specify the method of the requests in the command e.g. POST, GET, PUT
- KR helps out when you are asking the question ok I found this api endpoint now what?
- SHUBS has his owned currated word list under his "manually generated word list"
- Do not just pull one of the AN wordlist and think that's enough. Shubs says you should still be currating your own word list depending on your use case
- .net websites are not case sensitive so you can take out any entries in your word list that are specifically for case sensitive targets
- specifying the right word list is important as a .net word list probably won't be helpful on a linux server
