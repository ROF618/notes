# copy text in tmux session just using keyboard
1. `CTRL + A` #puts us into vi mode navigations mode
1. `SPACE` #allows highlighting of desired text
1. `ENTER` #saves highlighted text to clipboard
