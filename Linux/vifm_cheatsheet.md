vifm_cheatsheet

e - preview files in current pane
w - preview files in alternate pane
CTRL+w+s - changes split pane to horizontal
CTRL+w+v - changes split pane to vertical
CTRL+w+o - changes split pane to one pane
