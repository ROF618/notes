#bash_refreshers

###erases all whitespace characters

sed 's/ //g' file.txt

###erases all empty lines

sed '/^$/d' file.txt 

###delete everything after the first white space found on a line

sed 's/\s.*$//'

###replaces a line in each file of a directory with a new string

for file in ./*; do sed -i 's/^.*auth-user.*$/auth-user-pass .client.txt/' $file; done

###list all block devices and their mount points
lsblk

###erases all files that dont contain a specific string
find ./out1 -type f '!' -exec grep -q "my string" {} \; -delete;

###reading arrays
read -a test_array

for i in ${test_array[@]}
do
	echo "The number inputed is $i"
done

###switch cases
echo "Enter a number between 1 and 4. Type 'quit' to exit"

while [ 1 ]
do
	read mynum
	case $mynum in
		1) echo "Its a one"
			;;
		2) echo "Its a two"
			;;
		3) echo "Its a three"
			;;
		4) echo "Its a four"
			;;
		"quit") echo "You quit the program!"
			break
			;;
		*) echo "Oops, its not allowed"
		;;
	esac
done

###rename files with different file extensions
for f in $(find . -type f); do mv $f ${f}.jpg; done

###pull text between two tags
grep -r "<loc>" out_200/ | sed -n 's:.*<loc>\(.*\)</loc>.*:\1:p' ##loc is the example tag used

###get SUID files (files that can be run by low priv user put has some root capabilities)
find / -perm -u=s -type f2>/dev/null

###zero out drive
dd if=/dev/zero of=/dev/sdx bs=512

###date format
date -u +%Y%m%d%H%M%S

###tar unzip .tar.gz files
tar -xvf (x: extract, v: verbose, f:file)

# show markdown files with lynx so that you can view and interact with the file interactively
pandoc file.md | lynx -stdin
=======
###show markdown files with lynx so that you can view and interact with the file interactively

###check file system of hard drives
df -Th

### check for what is the syntax to format terminal prompt
* man bash
	-search for PROMPTING

### entr
for python the following syntax should be followed. `echo $FILE.py | entr -r python $FILE.py`

### show font family
`fc-list : family style | grep -i {name}`
