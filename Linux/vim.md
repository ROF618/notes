vim

:%!cmd - pipe current buffer into stdin of terminal emulator

hit !! which allows you to run a shell command and the output its pushed to your current vim instance

<leader>s - toggle spell check
z= - find suggestions on misspelled words

### removing all whitespace from selected text
* You could go into visual mode (by typing v while in command mode) and then select the required text and thereafter enter the command mode (by typing ":"). This will automatically insert the selection range and then you could perform the necessary substitution.
    - Go to visual mode by typing v
    - Select the necessary text.
    - Type : to enter command mode. You would find in the prompt below `:'<,'>`
    - The final command would look like this `:'<,'>s/ /_/g`

### remove all the whitespace from the document
* `:%s/\s\+$//g`
    - the command might need to be edited as the command that worked on fedora was `:%s/\s\+//`
    - got this solution from https://coderwall.com/p/m4083g/removing-whitespace-with-vim

### pipe ranges of line numbers to a command
* :n1, n2 !{command} e.g. :65, 104 !lb_counter

# Jump to the end of a function
* `%i` jumps to the end of the function
* highlight function `va[` 

# jump to the first character `_`
