#4/5/2021 1st lesson

- VODs will be kept on twitch for 60 days
- main content will be only on github.com/rwxrob/boost

#6/5/2021

- Implement the scientific method into your learning
    - doesn't need to be super formal when implementing to your learning method
           1.Make an observation.
           .Ask a question.
           .Form a hypothesis, or testable explanation.
           .Make a prediction based on the hypothesis.
           .Test the prediction.
           .Iterate: use the results to make new hypotheses or predictions.

- Read write execute
    - reading for scanning is important when dealing with technological information
    - sumarize all things you've learned so you have a condensed version to verify what you know
    - drilling is what makes you learn; if you don't practice it you won't learn it

- Zettelkasten
    - get your idea out on paper as fast as you can
        - use a unique identifier e.g. 20210506192950 (year-month-day-hour GMT-minute-secon)
    - limit your Zettels to 20 to 30 lines

