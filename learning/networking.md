networking


# Topology

## point-to-point
* only involves two machines and their direct connection

## bus
* no central network component to control processes
* all hosts are connected via one transmission medium
* only one hosts can send and all other hosts can only receive and evaluate whether the data is for them or not

## star
* all hosts maintain a connection to a central network component
* each hosts is connected via separate links usually in the form of a router, a hub, or a switch

## ring
* physical ring topology has two cables, one for input and one for output
* logical ring is based on a physical star topology
	- the central station usually issues a token in order to retrieve or send data sequentially from machine to machine
	- tokens pass through a ring network in one direction which works in accordance with the claim token process

## mesh
* fully meshed structure allows important networking nodes to meshed together so if one goes down the others are not affected
* partially meshed structure connects endpoints through a single connection

## tree
* extended star topology. Useful when combining server topologies
* used in larger company buildings

## daisy chain
* based on physical arrangement of the nodes in which multiple hosts are connected to each other in series via physical cable
* different from the token procedure as the connection are physically sequential


# Proxy
* sits in the middle of a connection and acts as a mediator
* if the device does not sit in the middle of the connection e.g is able to read both incoming and outgoing traffic from sender and receiver then the device is technically a gateway and not a true proxy
* true proxy almost always operate on layer 7
	examples:
	- dedicated proxy / forward proxy
	- reverse proxy
	- transparent proxy

## forward proxy / dedicated proxy
* client makes a requests to a computer and that computer carries out the request

## reverse proxy
* opposite of forward proxy in that a connection is asked to listen on a port and then specified traffic is sent back through the listening port
* good way to avoid intrusion detection systems (IDS)

## transparent / non-transparent proxies
* transparent proxy are not visible to the client and they intercept the client's requests and act as a substitute instance
* non-transparent must inform the client of its existence and also uses special proxy configuration in order to communicate
