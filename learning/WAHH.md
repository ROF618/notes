WAHH


# Ch2

1. because if a attacker can use the other mechanisms to impersonate a user than that user is already compromised
2. a session is a data structure that tracks the stat of the users interaction with the app and session token is the way a server validates that a certain user is who they say they are
3. because some input needs to be sent to other application that might violate the whitelist rules
4. it could give you an endpoint that could later allow you to escalate your privildge to administrator; specifically submitting user input(of a lower privilidged user) to the admins in order to compromise their session when it is viewed
5. Yes. If it were not for Step 4, this mechanism would be robust in terms of filtering the specific items it is designed to block. However, because your input is decoded after the filtering steps have been performed, you can simply URL-encode selected characters in your payload to evade the filter:

%22>%3cscript>alert(%22foo%22)</script>

