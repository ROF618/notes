perl_tut

# 3 main data types in perl
1. scalars
2. arrays
3. hashes

# declaring variables
* declare variable with $ a the beginning
* must start with a letter or underscore

# when using quotes; use double quotes if you want to add the new line[\n] character

# when running the perl interpreter the -n flag creates a while loop that goes line by line until the file is finished
