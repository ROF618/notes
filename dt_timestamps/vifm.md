vifm

0:00 description of vifm
0:36 vifm docs overview
2:26 starting vifm and navigating inside vifm
3:53 quitting vifm and starting vifm with arguments
5:16 manipulating the display panes inside vifm
7:14 previewing a file inside vifm
11:00 showing hidden directories and files
12:15 renaming files
14:03 bookmarks
16:51 deleting, cutting, copying and pasting files
18:20 vifm's config files
23:18 outro and patreon thanks!

0:00 - 0:29 description of vifm
0:36 - 2:09 vifm docs overview
2:26 - 3:52 starting vifm and navigating inside vifm
3:53 - 4:51 quitting vifm and starting vifm with arguments
5:16 - 7:13 manipulating the display panes inside vifm
7:14 - 11:00 previewing a file inside vifm
11:00 - 11:56 showing hidden directories and files
12:15 - 14:02 renaming files
14:03 - 16:49 bookmarks
16:51 - 18:10 deleting, cutting, copying and pasting files
18:20 - 23:17 vifm's config files
23:18 - 24:06 outro and patreon thanks!
