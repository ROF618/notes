cover_letters

testing 14/4/2021 13:35 pm
testing 14/4/2021 13:16 pm
testing


#10-11-2020

Hello, my name is Christopher Bravo and I am interested in your Linux Analyst position you have posted on job.listings.cpanel.net. First and foremost I can not say enough about how awesome GNU/Linux is. I currently run Debian based distributions of GNU/Linux on all of my personal machines with Debian Buster running on my main machine. In my spare time I like tinkering with my distributions as I am always trying to find the quickest and most efficient way of using the power of GNU/Linux to complete my daily task. I've been attempting to contribute however I can to helping tech content creators and FOSS projects as I am largely self taught when it comes to technology. The few classes I have taken have been in software development. I took a Javascript class while I was living in Mexico City and a Python course taught at Cpanel through the Houston Linux Group. Both classes were open to the public and they were taught by highly knowledgable people that enjoy sharing their knowledge. The principles of FOSS are also huge motivators when it comes to any new project I begin, as I believe that transparency leads to better security. Recently I've become more involved within the cyber security world. 

Fixing things has always been something I enjoy. When I was in high school I rebuilt a 1984 El Camino and since then I've always tried to fix things myself when possible. I enjoy the process of trouble shooting. Few things make me happier than being able to resolve a problem. Don't get me wrong, I also understand how frustrating it can be when you need something to work and it doesn't. I think being able to empathize with someone that is frustrated helps resolve the problem faster. So I've always tried to keep that mentality when approaching problems at work.

I have recently started my cyber security journey through the world of bug bounties. Currently I am testing two public and one private program. Testing these programs is where I spend most of my time now. Any other free time I have, I spend learning new security testing techniques. The programs I currently test have understandably strict disclosure policies, but I can say that while testing these programs I have come across countless instances of Apache and Nginx webservers. So naturally I've been trying to learn as much as possible about both technologies. I'm always trying to learn new things especially when it comes to the world of technology as it is constantly changing. Sometimes it can feel like trying to drink water from a fire hose, but when I start to use a new technology I've learned it makes it worth it. 

I would like to end by saying thank you to the reader of this, and another thank you to everyone at Cpanel. I have to say although I wasn't talkative during many of the hLug meetings I enjoyed them a lot. I can't wait for this Covid nightmare to end so that it is safe to go back to Cpannel on Wednesday nights. Thank you for your consideration and I hope to hear from you.
